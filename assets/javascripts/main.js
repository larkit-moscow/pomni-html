$(function() {
  var addLatest, clearSession, getLatest, goBack, handleSessionDuration, hideThisShowThis, highlightRoom, log, minifySessionData, onHide, onShow;
  log = function() {
    return console.log.apply(console, arguments);
  };
  $('document').on('tap', 'a', function(e) {
    return e.preventDefault();
  });
  $('*:not(.swiper-container)').on('dragstart drop', function() {
    return false;
  });
  clearSession = function() {
    window.session = {
      stand_id: 0,
      timestamp: 0,
      duration: 0,
      clicks_amount: 0,
      gallery_touches: 0,
      specialmode_toggles: 0,
      article_theme_toggles: 0,
      article_plus_toggles: 0,
      article_minus_toggles: 0,
      views_help: 0,
      views_gallery: 0,
      views_article: 0,
      views_catalog: 0,
      views_map: 0,
      views_organizers: 0,
      views_zoom: 0,
      views_attention: 0,
      views_preview: 0
    };
    window.session.stand_id = $('html').get(0).getAttribute('data-stand-id') || void 0;
    return window.session.timestamp = +new Date();
  };
  clearSession();
  window.clearStats = function() {
    return localStorage.removeItem('stats');
  };
  handleSessionDuration = function() {
    var CURRENT, LAST, TOTAL;
    LAST = window.session.timestamp;
    CURRENT = +new Date();
    TOTAL = CURRENT - LAST;
    return window.session.duration = TOTAL;
  };
  minifySessionData = function(session) {
    var LOCALSTORAGE_LATEST, NEW, OBJ_KEYS, TEMP_ARY, key;
    TEMP_ARY = [];
    OBJ_KEYS = [];
    for (key in session) {
      OBJ_KEYS.push(key);
      if (session.hasOwnProperty(key)) {
        TEMP_ARY.push(session[key]);
      }
    }
    handleSessionDuration();
    LOCALSTORAGE_LATEST = localStorage.getItem('stats');
    NEW = [LOCALSTORAGE_LATEST, TEMP_ARY.join(',')].join(';');
    localStorage.setItem('stats', NEW);
    return clearSession();
  };
  window.returnBackToEnterIn3MinutesStay = function() {
    clearTimeout(window.returnBackToEnterIn3MinutesStayTimer);
    return window.returnBackToEnterIn3MinutesStayTimer = setTimeout(function() {
      hideThisShowThis(window.current, $('#Enter'), true, false);
      return minifySessionData(session);
    }, 180000);
  };
  $(document).on('tap', function() {
    returnBackToEnterIn3MinutesStay();
    return window.session.clicks_amount++;
  });
  $('[data-open-help]').on('tap', function() {
    var $current, src;
    $current = $(window.current);
    if ($current.length && $current.attr('data-help')) {
      src = $current.attr('data-help');
      return $('.help').css('background-image', "url(" + src + ")").fadeIn(function() {
        return window.session.views_help++;
      });
    }
  });
  $('.help').on('tap', function() {
    return $(this).fadeOut();
  });
  window.current = document.getElementById('Enter');
  window.disableEnterVideo = function() {
    if (!window.intro_video) {
      window.intro_video = $('#Enter video').remove();
    }
    return $('#Catalog .swiper-wrapper').removeAttr('style');
  };
  window.enableEnterVideo = function() {
    if (window.intro_video) {
      $('#Enter').prepend(window.intro_video).find('video').get(0).play();
      return delete window.intro_video;
    }
  };
  window.onHideEnter = function() {
    $('#Footer').queue(function() {
      $(this).css({
        opacity: 1,
        bottom: -85
      });
      $(this).show();
      return $(this).dequeue();
    }).animate({
      bottom: 0
    }, {
      duration: 900,
      easing: 'easeOutBounce'
    });
    $('#Header').queue(function() {
      $(this).css({
        opacity: 1,
        top: -125
      });
      $(this).show();
      return $(this).dequeue();
    }).animate({
      top: 0
    }, {
      duration: 900,
      easing: 'easeOutBounce'
    });
    return disableEnterVideo();
  };
  window.onShowEnter = function() {
    window.history_ary = [];
    $('body').removeClass('specialmode');
    $('[id^="Article"]').addClass('article__dark');
    $('#Footer').queue(function() {
      $(this).css('opacity', 1);
      return $(this).dequeue();
    }).fadeOut(450);
    $('#Header').queue(function() {
      $(this).css('opacity', 1);
      return $(this).dequeue();
    }).fadeOut(450);
    return enableEnterVideo();
  };
  window.onShowAttention = function(el, caller) {
    $('[data-open-help]').addClass('hidden');
    $('[data-open-catalog]').addClass('hidden');
    return window.session.views_attention++;
  };
  window.onHideAttention = function(el, caller) {
    return $('[data-open-help]').removeClass('hidden');
  };
  window.onShowCatalog = function() {
    if (!window.catalog_swiper) {
      window.catalog_swiper = new Swiper('#Catalog .swiper-container', {
        mode: 'horizontal',
        slidesPerView: 'auto',
        preventLinksPropagation: true,
        freeMode: true,
        freeModeFluid: true,
        scrollbar: {
          container: '#Catalog .swiper-scrollbar',
          draggable: false,
          hide: false
        }
      });
    } else {
      window.catalog_swiper.reInit();
    }
    $('[data-open-catalog]').fadeOut();
    return window.session.views_catalog++;
  };
  window.onHideCatalog = function() {
    return $('[data-open-catalog]').removeClass('hidden').fadeIn();
  };
  window.onShowOrganizers = function() {
    $('#Organizers [data-kinetic]').get(0).scrollTop = 0;
    $('#Organizers [data-kinetic]').kinetic('attach', {
      x: false
    });
    return window.session.views_organizers++;
  };
  window.onHideOrganizers = function() {
    return $('#Organizers [data-kinetic]').kinetic('detach');
  };
  window.onShowPreview = function(el, caller) {
    var initial_slide;
    if (caller) {
      initial_slide = caller.getAttribute('data-id');
    } else if (window.last_seen_preview_index) {
      initial_slide = window.last_seen_preview_index;
    } else {
      initial_slide = 0;
    }
    if (!window.preview_swiper) {
      window.preview_swiper = new Swiper('#Preview .swiper-container', {
        mode: 'horizontal',
        slidesPerView: '1',
        initialSlide: initial_slide,
        preventLinksPropagation: true,
        onlyExternal: true,
        onSwiperCreated: function(swiper) {
          return swiper.swipeTo(initial_slide, 0);
        },
        scrollbar: {
          container: '#Preview .swiper-scrollbar',
          draggable: false,
          hide: false
        }
      });
      $('#Preview .preview__prev').on('click', function(e) {
        e.preventDefault();
        return preview_swiper.swipePrev();
      });
      $('#Preview .preview__next').on('click', function(e) {
        e.preventDefault();
        return preview_swiper.swipeNext();
      });
    } else {
      window.preview_swiper.swipeTo(initial_slide, 0);
    }
    return window.session.views_preview++;
  };
  window.onHidePreview = function(el, caller) {};
  window.onShowGallery = function(el, caller, id) {
    var gallery_id, initial_slide;
    if (typeof caller === 'object') {
      initial_slide = caller.getAttribute('data-photo-id') || 0;
      gallery_id = '#Gallery' + caller.getAttribute('data-id');
      window.last_seen_preview_index = caller.getAttribute('data-section');
    } else {
      initial_slide = window.last_seen_gallery_photo || 0;
      gallery_id = id;
    }
    window.gallery_swiper = new Swiper(gallery_id + ' .swiper-container', {
      mode: 'horizontal',
      slidesPerView: 'auto',
      centeredSlides: true,
      initialSlide: initial_slide,
      onSwiperCreated: function(swiper) {
        return swiper.swipeTo(initial_slide, 0);
      },
      onTouchEnd: function() {
        return window.session.gallery_touches++;
      },
      scrollbar: {
        container: gallery_id + ' .swiper-scrollbar',
        draggable: false,
        hide: false
      }
    });
    return window.session.views_gallery++;
  };
  window.onHideGallery = function(el, caller) {};
  window.onShowZoom = function(el, caller) {
    var image, title;
    if (caller) {
      window.lastZoomCaller = caller;
    } else {
      caller = window.lastZoomCaller;
    }
    image = caller.getAttribute('data-image');
    $('[data-zoom-image]').attr('src', image);
    title = caller.getAttribute('data-title');
    window.last_seen_gallery_photo = caller.getAttribute('data-photo-id');
    $('#Zoom .kinetic').get(0).scrollTop = 0;
    $('#Zoom .kinetic').kinetic('attach', {
      x: false
    });
    $('#Zoom .zoom__title span').text(title);
    return window.session.views_zoom++;
  };
  window.onHideZoom = function(el, caller) {
    return $('#Zoom .kinetic').kinetic('detach');
  };
  window.onShowArticle = function(el, caller) {
    var article_id, kinetic;
    if (caller) {
      article_id = caller.getAttribute('data-id');
      window.last_seen_preview_index = caller.getAttribute('data-section');
      $('#Article' + article_id + ' .article').get(0).scrollTop = 0;
      kinetic = $('#Article' + article_id + ' .article').kinetic('attach', {
        x: false,
        triggerHardware: true,
        throttleFPS: 30
      });
    }
    return window.session.views_article++;
  };
  window.onHideArticle = function(el, caller) {
    var article_id;
    if (caller) {
      article_id = caller.getAttribute('data-id');
    }
    return $('#Article' + article_id + ' .article').kinetic('detach');
  };
  $('[data-scroll-top]').on('tap', function(e) {
    return $(this).closest('[data-page]').find('.article').animate({
      scrollTop: 0
    }, 500);
  });
  highlightRoom = function(room_id) {
    var active, room;
    room = $('g[id^="Room' + room_id + '"]').get(0);
    active = room.getAttribute('class') === 'active';
    if (!active) {
      $('g.active[id^="Room"]').removeAttr('class');
      return room.setAttribute('class', 'active');
    }
  };
  $('g[id^="Room"]').on('tap', function() {
    var id;
    id = parseInt(this.id.replace('Room', '')) - 1;
    return window.map_swiper.swipeTo(id);
  });
  window.onShowMap = function() {
    var initial;
    window.session.views_map++;
    initial = parseInt(document.getElementsByTagName('html')[0].getAttribute('data-stand-id')) - 1;
    $('g[id="Room' + (initial + 1) + '"]').get(0).setAttribute('data-current', true);
    if (!window.map_swiper) {
      window.map_swiper = $('#Map .swiper-container').swiper({
        mode: 'horizontal',
        slidesPerView: 'auto',
        centeredSlides: true,
        speed: 500,
        initialSlide: initial,
        onSlideChangeStart: function(swiper) {
          var id;
          id = swiper.activeIndex + 1;
          return highlightRoom(id);
        },
        onFirstInit: function(swiper) {
          return highlightRoom(initial + 1);
        }
      });
    } else {
      window.map_swiper.swipeTo(initial, 0);
    }
    $('[data-map-prev]').on('tap', function() {
      return window.map_swiper.swipePrev();
    });
    return $('[data-map-next]').on('tap', function() {
      return window.map_swiper.swipeNext();
    });
  };
  if (window.history_ary == null) {
    window.history_ary = [];
  }
  goBack = function(current) {
    var $current, $previous, previous;
    $current = $(current);
    if ((previous = window.history_ary.pop())) {
      $previous = $('#' + previous);
      return hideThisShowThis($current, $previous, true, false, '#' + previous);
    } else {
      return console.log('Nothing to go');
    }
  };
  addLatest = function(id) {
    var history;
    history = window.history_ary;
    return history.push(id);
  };
  getLatest = function() {
    var length;
    if ((length = window.history_ary.length)) {
      return window.history_ary[length - 1];
    } else {
      console.log('getLatest :: no history there');
      return false;
    }
  };
  onHide = function(el, caller) {
    var glob;
    glob = el.getAttribute('data-onhide');
    if (window.hasOwnProperty(glob)) {
      return window[glob](el, caller);
    }
  };
  onShow = function(el, caller, id) {
    var glob;
    glob = el.getAttribute('data-onshow');
    if (window.hasOwnProperty(glob)) {
      window[glob](el, caller, id);
    }
    if (!el.hasAttribute('data-help')) {
      return $('[data-open-help]').css('opacity', '.2');
    } else {
      return $('[data-open-help]').css('opacity', '1');
    }
  };
  $(document).on('click', '[data-close]', function() {
    if (!window.animating) {
      return goBack.call(null, window.current);
    }
  });
  hideThisShowThis = function(hide, show, omit_history_push, caller, id) {
    var $current, $next;
    $next = $(show);
    $current = $(hide);
    if ($next.is($current)) {
      return;
    }
    window.animating = true;
    return $current.fadeOut(500, function() {
      $(this).removeAttr('data-current');
      window.previous = this;
      if (!omit_history_push) {
        console.log('Adding latest to:', this.id);
        addLatest(this.id);
      }
      onHide(this);
      return $next.queue(function() {
        window.current = this;
        $(this).css('opacity', 0);
        $(this).show();
        onShow(this, caller, id);
        $(this).attr('data-current', true);
        return $(this).dequeue();
      }).animate({
        opacity: 1
      }, 500, function() {
        return window.animating = false;
      });
    });
  };
  $(document).on('tap', '[data-show]', function() {
    var $current, $next, args;
    if (!window.animating) {
      $next = $(document.getElementById(this.getAttribute('data-show')));
      $current = $('#' + window.current.id);
      args = arguments;
      if ($next.length) {
        return hideThisShowThis($current, $next, false, this);
      }
    }
  });
  $('[data-contrast]').on('tap', function(e) {
    $('body').toggleClass('specialmode');
    return window.session.specialmode_toggles++;
  });
  return $('[data-geo-close]').on('tap', function(e) {
    if (window.current.id === 'Map') {
      return $('#Map [data-close]').trigger('click');
    }
  });
});
